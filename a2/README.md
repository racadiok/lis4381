# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Assignment 2 Requirements:

   1. Create a mobile recipe app using Android Studio.

#### README.md file should include the following items:

   1. Course title, your name, assignment requirements, as per A1;
   2. Screenshot of running application's first user interface;
   3. Screenshot of running application's second user interface;

#### Assignment Screenshots:

Screenshot of running application's first user interface:

![image 1](images/healthyrecipe1.png)

Screenshot of running application's second user interface:

![image 2](images/healthyrecipe2.png)