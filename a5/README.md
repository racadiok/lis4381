# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Assignment 5 Requirements:

1. Requires **A4** cloned files.
2. Review subdirectories and files.
3. Open **index.php** and review code.

#### README.md file should include the following items:

1. **Course title, your name, assignment requirements, as per A5;**
2. Screenshots as per below examples;
3. Link to **local lis4381 web app:** (http://localhost/repos/lis4381/);


#### Assignment Screenshots:

**index.php**:

![image 1](img/index.png)


***add_petstore_process.php (that includes error.php)**

![image 2](img/error.png)