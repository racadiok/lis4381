> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### Assignment 1 Requirements:

*Three Parts:*

  1. Distributed Version Control with Git and Bitbucket
  2. Development Installations
  3. Chapter Questions (Chs 1, 2)

### README.md file should include the following items:

  * Screenshot of AMPPS installation [My PHP Installation](http://localhost "My PHP Installation");
  * Screenshot of running java Hello;
  * Screenshot of running Android Studio - My First App
  * git commands w/short descriptions;
  * Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).
  
> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

  1. git init - creates new Git repository
  2. git status - displays the state of the working directory and staging area
  3. git add - adds file contents to the index
  4. git commit - used to save your changes to the local repository
  5. git push - used to upload local repositorycontent to a remote repository
  6. git pull - fetch from and integrate with another repository or local branch
  7. git stash - stash changes in a dirty working directory
  
#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost)*

![image 1](images/PHP.png)

*Screenshot of running java Hello:*

![image 2](images/javaHello.png)

*Screenshot of Android Studio - My First App:*

![image3](images/AndroidStudioMyFirstApp.png)

### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")