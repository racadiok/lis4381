# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Assignment 4 Requirements:

1. Clone assignment starter files
2. Open index.php and customize portfolio

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A4;
2. Screenshot of LIS4381 portal (main page);
3. Screenshot of failed validation;
4. Screenshot of passed validation;

#### Assignment Screenshots:

Screenshot of LIS4381 portal:

![image 1](img/1.png)

Screenshot of failed validation:

![image 2](img/2.png)

Second Screenshot of failed:

![image 3](img/3.png)

Screenshot of passed validation:

![image 3](img/4.png)