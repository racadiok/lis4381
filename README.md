# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
        
2. [A2 README.md](a2/README.md)
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
        
3. [A3 README.md](a3/README.md)
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Create My Event Ticket Value Android app
    - Provide screenshot of running applications first interface
    - Provide screenshot of running applications first interface
        
4. [P1 README.md](p1/README.md)
    - Create a launcher icon image and display it in both activities (screens)
    - Must add background color(s) to both activities
    - Must add border around image and button
    - Must add text shadow (button)
        
5. [A4 README.md](a4/README.md)
    - Clone assignment starter files
    - Open index.php and customize portfolio
        
6. [A5 README.md](a5/README.md)
    - Requires **A4** cloned files.
    - Review subdirectories and files.
    - Open **index.php** and review code.
        
7. [P2 README.md](p2/README.md)
    - Requires A4 cloned files.
    - Review subdirectories and files.
    - Open index.php and review code:
    - Suitably modify meta tags.
    - Change title, navigation links, and header tags appropriately.
    - See videos for complete development.
    - Turn off client-side validation.
    - Add server-side validation and regular expressions.