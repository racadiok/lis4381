# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Project 1 Requirements:

Backward-Engineer the screenshots below, using *your* photo, contact information, and interests.

*Research how to do the following requirements (see screenshots below):*

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per P1;
2. Screenshot of running application’s first user interface;
3. Screenshot of running application’s second user interface;

#### Assignment Screenshots:

Screenshot of Business Card's first user interface:

![image 1](images/bc.png)

Screenshot of Business Card's second user interface:

![image 2](images/bc2.png)
