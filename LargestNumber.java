import java.util.Scanner;

public class LargestNumber {

    public static void main(String[] args) {

        Scanner kelly = new Scanner(System.in);

        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
        System.out.println();

        System.out.println("Enter first integer: ");
        int num = kelly.nextInt();
        
        System.out.println("Enter second integer: ");
        int num2 = kelly.nextInt();

        if(num1 > num2){
            System.out.println(num1 + " is larger than " + num2);
        }
        else if (num2 > num1){
            System.out.println(num2 + " is larger than " + num1);
        }
        else if (num1 == num2){
            System.out.println("Integers are equal.");
        }
    }
    
}
