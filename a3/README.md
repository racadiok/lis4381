# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Assignment 3 Requirements:

1. Create ERD based upon business rules
2. Provide screenshot of completed ERD
3. Create My Event Ticket Value Android app
4. Provide screenshot of running applications first interface
5. Provide screenshot of running applications first interface

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Links to the following files:
a. a3.mwb
b. a3.sql

#### Assignment Screenshots:

Screenshot of ERD:

![image 1](images/erd.png)

Screenshot of running application's first user interface:

![image 2](images/myevent1.png)

Screenshot of running application's first user interface:

![image 3](images/myevent2.png)