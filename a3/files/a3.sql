-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema root
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `root` ;

-- -----------------------------------------------------
-- Schema root
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `root` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `root` ;

-- -----------------------------------------------------
-- Table `root`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `root`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `root`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `root`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `root`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `root`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `root`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Paws and Claws', 'Main Street', 'New York City', 'NY', 45634, 4193458294, 'pawsandclaws@gmail.com', 'pawsandclaws.com', 12345678.90, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Petco', 'Coral Reef Street', 'Port Saint Lucie', 'FL', 34983, 7723456789, 'petcopsl@yahoo.com', 'petco.org', 84729841.94, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', 'Sago Drive', 'Jensen Beach', 'FL', 34957, 7722445434, 'furryfriendsjensenbeach@gmail.com', 'furryfriends.com', 74837193.69, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Animal Tails', 'Saint Andrews Drive', 'Palm City', 'FL', 34990, 7722331456, 'animaltailspc@hotmail.com', 'animaltails.com', 73928471.67, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetSmart', 'Edwards Street', 'Tallahassee', 'FL', 32304, 8503459008, 'petsmart850@bellsouth.net', 'petsmart.com', 91738563.99, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Supermarket', 'Woodward Avenue', 'Tallahassee', 'FL', 32301, 8506849381, 'petsupermarket45563@yahoo.com', 'petsupermarket.com', 18374852.80, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Animal World', 'Traditions Way', 'Tallahassee', 'FL', 32303, 8504445665, 'animalworldtraditions@gmail.com', 'animalworld.org', 20471854.33, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Puppy Palace', 'Phillips Road', 'Tallahassee', 'FL', 32311, 8503425009, 'puppypalacetally@gmail.com', 'puppyplace.com', 63547832.25, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Bark Avenue', 'Croquet Street', 'Port Saint Lucie', 'FL', 34983, 7728803004, 'barkavenuepsl@gmail.com', 'barkavenue.com', 85489732.54, NULL);
INSERT INTO `root`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'A Reptile Adventure', 'Chapel Drive', 'Tallahassee', 'FL', 32304, 8506773345, 'reptilestally@yahoo.com', 'reptileadventure.com', 74927592.56, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bob', 'Brown', 'Tennessee Street', 'Tallahassee', 'FL', 32304, 8503729474, 'bobbrown1967@yahoo.com', 40, 450, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Billy', 'Smith', 'Cameleon Avenue', 'Tallahassee', 'FL', 32303, 7723456743, 'smithb4567@gmail.com', 400, 436.28, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sara', 'Jones', 'Main Street', 'Tallahassee', 'FL', 32301, 7276341241, 'jonessar1234@gmail.com', 9999.99, 322, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Trisha', 'Langford', 'Pensacola Street', 'Tallahassee', 'FL', 32302, 3056674324, 'langfordt@yahoo.com', 5, 5643.76, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Lafonda', 'Ram', 'Day Street', 'Tallahassee', 'FL', 32034, 5612334235, 'ramlafonda@hotmail.com', 127.87, 7584.43, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Chad', 'Rising', 'Lipona Road', 'Tallahassee', 'FL', 32306, 7509785598, 'risingchad@bellsouth.net', 20, 12, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Brad', 'Martini', 'Appalachee Parkway', 'Tallahassee', 'FL', 32314, 8504453676, 'martinib56@gmail.com', 25, 43, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Tiffany', 'Hayes', 'Park Avenue', 'Tallahassee', 'FL', 32311, 5613426543, 'hayesttt3t@yahoo.com', 10, 654.21, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Roger', 'Alexis', 'Phillips Road', 'Tallahassee', 'FL', 32311, 2314343356, 'alexiss3rogerr@yahoo.com', 1567.25, 76.99, NULL);
INSERT INTO `root`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Floyd', 'Taspek', 'Saint Andrews Drive', 'Tallahassee', 'FL', 32304, 8507786754, 'floydt1985@gmail.com', 25.99, 99.99, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `root`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `root`;
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 5, 'German Shepard', 'm', 374, 400, 1, 'black/brown', '2009-07-05', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'Border Collie', 'm', 200, 300, 4, 'black/white', NULL, 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'Pitbull Mix', 'm', 199, 200, 2, 'black', '2011-12-24', 'y', 'n', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 6, 'Tree Frog', 'f', 45, 75, 2, 'green', '2005-08-01', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, NULL, 'Maine Coon', 'f', 69, 100, 5, 'grey/brown', NULL, 'n', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 4, 'Guniea Pig', 'm', 54, 85, 4, 'brown/white', '2010-11-12', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 4, 'Porcupine', 'f', 89, 100, 7, 'brown', '2007-09-19', 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 1, 'Chiuaua', 'm', 108, 150, 8, 'tan', NULL, 'n', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'Iguana', 'f', 250, 300, 2, 'green', NULL, 'y', 'y', NULL);
INSERT INTO `root`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 1, 'Black Lab', 'f', 345, 400, 1, 'black', '2018-10-13', 'y', 'y', NULL);

COMMIT;

