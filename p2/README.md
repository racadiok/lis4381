# LIS4381 - Mobile Web Application Development

## Kelly Racadio

### LIS4381 Project 2 Requirements:

1. Requires A4 cloned files.
2. Review subdirectories and files.
3. Open index.php and review code:
4. Suitably modify meta tags.
5. Change title, navigation links, and header tags appropriately.
6. See videos for complete development.
7. Turn off client-side validation.
8. Add server-side validation and regular expressions.

#### README.md file should include the following items:

1. **Course title, your name, assignment requirements, as per P2;**
2. Screenshots as per below examples;

#### Assignment Screenshots:

Screenshot of index.php:

![image 1](images/1.png)
![image 2](images/2.png)

Screenshot of edit_petstore.php:

![image 3](images/3.png)
![image 4](images/4.png)

Screenshot of edit_petstore_process.php (that includes error.php):

![image 5](images/5.png)

Screenshot of Carousel (Home Page):

![image 6](images/6.png)